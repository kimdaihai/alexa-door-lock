'use strict';

var Alexa = require('alexa-sdk');
var mqtt = require('mqtt');
var APP_ID = undefined;

exports.handler = function(event, context, callback) {
    var alexa = Alexa.handler(event, context);
    alexa.APP_ID = APP_ID;
    // To enable string internationalization (i18n) features, set a resources object.
    alexa.resources = languageStrings;
    alexa.registerHandlers(handlers);
    alexa.execute();
};


var handlers = {
    'LaunchRequest': function () {
        this.attributes['speechOutput'] = this.t("WELCOME_MESSAGE", this.t("SKILL_NAME"));
        // If the user either does not reply to the welcome message or says something that is not
        // understood, they will be prompted again with this text.
        this.attributes['repromptSpeech'] = this.t("WELCOME_REPROMPT");
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    },
    'LockUnlock': function () {
        var itemSlot = this.event.request.intent.slots.DoorStates; //'DoorStates' is the slot name
        var itemName;
  
        if (itemSlot && itemSlot.value) {
            itemName = itemSlot.value.toLowerCase();
        
            var self = this; //for using 'this' inside the callbacks

		    // Pushlish to mqtt broker
            var mqttPromise = new Promise(function(resolve, reject) {
            var options = {
                host: 'YOUR_MQTT_SERVER',
                port: 1234,
                clientId: 'alexa_door_' +  Math.random().toString(16).substr(2, 8),
                username: 'YOUR_PASSWORD',
                password: 'YOUR_MQTT_PASSWORDD'
                };

            var client = mqtt.connect(options);

            client.on('connect', function() { 
                // When connected to mqtt broker,
                // publish a message with the topic 'DoorLock'
                    client.publish('DoorLock', itemName, function() {
                        client.end();
                        resolve('published succesfully');
                    });
                });
            });

            mqttPromise.then(
                //resolve
                function(data) {
                    console.log(data);				
                    self.attributes['speechOutput'] = self.t("CHANGE_STATE") + itemName + "ed";
                    self.emit(':tellWithCard', self.attributes['speechOutput'], self.attributes['speechOutput'], "Door " + itemName + "ed");
                },
                //reject
                function(err) {
                    console.log('An error occurred:', err);
                     self.attributes['speechOutput'] = self.t("ERROR");
                    self.emit(':tell', self.attributes['speechOutput']);
                });
        }
        else
        {
            this.attributes['speechOutput'] = this.t("REPEAT");
            this.emit(':ask', this.attributes['speechOutput']); 
        }
    },
    'AMAZON.HelpIntent': function () {
        this.attributes['speechOutput'] = this.t("HELP_MESSAGE");
        this.attributes['repromptSpeech'] = this.t("HELP_REPROMPT");
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    },
    'AMAZON.RepeatIntent': function () {
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    },
    'AMAZON.StopIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'AMAZON.CancelIntent': function () {
        this.emit('SessionEndedRequest');
    },
    'SessionEndedRequest':function () {
        this.emit(':tell', this.t("STOP_MESSAGE"));
    },
    'Unhandled': function () {
        this.attributes['speechOutput'] = this.t("HELP_MESSAGE");
        this.attributes['repromptSpeech'] = this.t("HELP_REPROMPT");
        this.emit(':ask', this.attributes['speechOutput'], this.attributes['repromptSpeech'])
    }
};




var languageStrings = {
    "en": {
        "translation": {
            "SKILL_NAME": "Door Lock",
            "WELCOME_MESSAGE": "Welcome to the Door Lock skill, You can ask the door to lock or unlock itself.",
            "WELCOME_REPROMPT": "Ask to door lock or unlock itself.",
            "DISPLAY_CARD_TITLE": "%s  - Change state to %s ed.",
			"CHANGE_STATE": "The door is now ",
            "HELP_MESSAGE": "You can ask the door to lock or unlock itself",
            "REPEAT" :"Please repeat your request.",
            "HELP_REPROMPT": "You can say things like, ask to door lock itself... Now, what can I help you with?",
            "ERROR" :"There was a problem with your request. Please try again later.",
            "STOP_MESSAGE": "Goodbye!",
        }
    }
};


